package com.company;

public class LinkedList<T> {

    private boolean containerEmpty = true;
    private Node<T> current = new Node<>();
    private Node<T> head;
    private Node<T> curr; // for method next() and prev()
    private int size = 0;

    public void add(T value) {
        Node<T> node = new Node<>();
        node.value = value;
        node.prev = current;
        node.idx = node.prev.idx + 1;
        current.next = node;
        current = node;

        if (containerEmpty) {
            head = current;
            curr = head.prev;
            containerEmpty = false;
        }

        this.size++;
    }

    public void prev() {
        curr = curr.prev;
        System.out.println(curr.value);
    }

    public void next() {
        curr = curr.next;
        System.out.println(curr.value);
    }

    public T get(int index) {
        current = head;
        checkIndex(index);

        while (current != null) {
            if (current.idx == index) {
                break;
            }
            current = current.next;
        }

        return current.value;
    }

    public int size() {
        return this.size;
    }

    public void put(T element, int index) {
        checkIndex(index);

        Node<T> node = new Node<>();
        node.value = element;
        node.idx = index;
        this.size++;

        current = head;
        while (current != null) {
            if (current.idx == index) {
                current.prev.next = node;
                node.prev = current.prev;
                current.prev = node;
                node.next = current;

                if (index == 0) {
                    head = node;
                }

                while (current != null) {
                    current.idx++;
                    current = current.next;
                }
                break;
            }
            current = current.next;
        }
    }

    public void remove(int index) {
        checkIndex(index);

        current = head;
        while (current != null) {
            if (current.idx == index) {
                current.prev.next = current.next;
                current.next.prev = current.prev;

                if (index == 0) {
                    head = current.next;
                }

                current = current.next;
                while (current != null) {
                    current.idx--;
                    current = current.next;
                }
                this.size--;
                break;
            }
            current = current.next;
        }
    }

    private void checkIndex(int index) {
        if (index > size - 1 || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }

}
