package com.company;

public class TestClass {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<>();
        list.add("Dima");
        list.add("Likes");
        list.add("JS");

        System.out.println("List get: " + list.get(0));
        System.out.println("List size: " + list.size());

        list.put("no", 0);
        System.out.println("List get: " + list.get(1));
        System.out.println("List size: " + list.size());

        list.next();
        list.next();
        list.prev();

        list.remove(0);
        System.out.println("List get: " + list.get(0));
        System.out.println("List size: " + list.size());

        list.next();
        list.next();
        list.prev();

    }
}
