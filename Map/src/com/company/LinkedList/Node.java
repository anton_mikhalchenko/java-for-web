package com.company.LinkedList;

public class Node<T> {
    public T value;
    public int idx = -1;

    public Node<T> next = null;

    public Node<T> prev = null;
}
