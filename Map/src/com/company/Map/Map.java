package com.company.Map;


import com.company.LinkedList.LinkedList;

public class Map<T, M> {

    private boolean containerEmpty = true;
    private int size = 0;

    private Node<T, M> current = new Node<>();
    private Node<T, M> head;

    public void put(T key, M value) {
        Node<T, M> node = new Node<>();
        node.key = key;
        node.value = value;
        current.next = node;
        node.prev = current;
        current = node;

        size++;

        if (containerEmpty) {
            head = current;
            containerEmpty = false;
        }
    }

    public M get(T key) {
        current = head;

        while (current != null) {
            if (current.key.equals(key)) {
                break;
            }
            current = current.next;
        }

        return current.value;
    }

    public int size() {
        return this.size;
    }

    public void remove(T key) {
        current = head;

        while (current != null) {
            if (current.key.equals(key)) {
                current.prev.next = current.next;
                current.next.prev = current.prev;
                size--;
                break;
            }
            current = current.next;
        }
    }

    public LinkedList<T> getKeys() {
        LinkedList<T> keysList = new LinkedList<>();
        current = head;

        while (current != null) {
            keysList.add(current.key);
            current = current.next;
        }

        return keysList;
    }
}
