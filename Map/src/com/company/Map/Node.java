package com.company.Map;

public class Node<T, M> {

    public T key;
    public M value;

    public Node<T, M> next = null;
    public Node<T, M> prev = null;
}
