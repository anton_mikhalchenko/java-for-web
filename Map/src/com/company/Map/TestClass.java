package com.company.Map;

import com.company.LinkedList.LinkedList;

public class TestClass {

    public static void main(String[] args) {
        Map<Integer, String> map = new Map<>();
        map.put(1, "Dima");
        map.put(2, "Anton");
        map.put(3, "Kolya");

        System.out.println(map.get(2));
        System.out.println(map.size());

        map.remove(1);
        System.out.println(map.size());

        System.out.println(map.get(2));

        LinkedList<Integer> keys;
        keys = map.getKeys();
        for (int i = 0; i < keys.size(); i++) {
            System.out.println(keys.get(i));
        }
    }
}
