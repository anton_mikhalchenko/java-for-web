package com.company.Scheme;

import com.company.Scheme.details.*;

public abstract class Drivable extends ControlPanel {

    private int maxPassengers;
    private int numberOfWheels;
    private String name;

    private ControlPanel controlPanel = new ControlPanel();
    private Accelerator accelerator = new Accelerator();
    private GasTank gasTank = new GasTank();
    private Horn horn = new Horn();
    private FrontWheels frontWheels = new FrontWheels();
    private RearWheels rearWheels = new RearWheels();
    private SteeringWheel steeringWheel = new SteeringWheel(frontWheels);
    private HandBrake handBrake = new HandBrake(frontWheels, rearWheels);
    private Engine engine = new Engine(gasTank, frontWheels, rearWheels);
    private BreakPedal breakPedal = new BreakPedal(frontWheels, rearWheels);

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void accelerate() {

        this.setSpeed(accelerator.accelerate(this.getSpeed(), engine));
    }

    public void startEngine() {
        engine.showStatus();
    }

    public void turnLeft() {
        steeringWheel.turnLeft();
    }

    public void turnRight() {
        steeringWheel.turnRight();
    }

}
