package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class Coupe extends Vehicle {

    public Coupe() {
        this.setSpeed(120);
        this.setName("Ford");
        this.setMaxPassengers(2);
        this.setDriveType("4WD");
        this.setNumberOfDoors(2);
        this.setNumberOfWheels(4);
        this.setSize(100);
    }
}
