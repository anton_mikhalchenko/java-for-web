package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class Hatchback3Doors extends Vehicle{

    public Hatchback3Doors() {
        this.setSpeed(120);
        this.setName("BMW");
        this.setMaxPassengers(5);
        this.setDriveType("4WD");
        this.setNumberOfDoors(3);
        this.setNumberOfWheels(4);
        this.setSize(123);
    }
}
