package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class Hatchback5Doors extends Vehicle {

    public Hatchback5Doors() {
        this.setSpeed(120);
        this.setName("Ford");
        this.setMaxPassengers(5);
        this.setDriveType("4WD");
        this.setNumberOfDoors(4);
        this.setNumberOfWheels(4);
        this.setSize(123);
    }
}
