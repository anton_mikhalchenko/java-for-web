package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class Motorcycle extends Vehicle {

    public Motorcycle() {
        this.setSpeed(150);
        this.setName("Yamaha");
        this.setMaxPassengers(5);
        this.setDriveType("4WD");
        this.setNumberOfDoors(0);
        this.setNumberOfWheels(2);
        this.setSize(50);
    }
}
