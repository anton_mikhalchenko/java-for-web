package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class SUV extends Vehicle {

    public SUV() {
        this.setSpeed(120);
        this.setName("Fiat");
        this.setMaxPassengers(7);
        this.setDriveType("4WD");
        this.setNumberOfDoors(6);
        this.setNumberOfWheels(4);
        this.setSize(150);
    }
}
