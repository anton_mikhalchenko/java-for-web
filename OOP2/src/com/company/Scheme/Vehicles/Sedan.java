package com.company.Scheme.Vehicles;

import com.company.Scheme.Vehicle;

public class Sedan  extends Vehicle{

    public Sedan() {
        this.setSpeed(120);
        this.setName("BMW");
        this.setMaxPassengers(5);
        this.setDriveType("4WD");
        this.setNumberOfDoors(4);
        this.setNumberOfWheels(4);
        this.setSize(123);
    }
}
