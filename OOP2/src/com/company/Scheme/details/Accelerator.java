package com.company.Scheme.details;

public class Accelerator implements StatusAware{

    private int speed = 0;

    public int accelerate(int speed, Engine engine) {
        if (engine.works()) {
            this.speed = speed + 10;
            showStatus();
            engine.showStatus();
            return speed += 10;
        }
        else {
            engine.showStatus();
            return speed -= 20;
        }
    }

    @Override
    public void showStatus() {
        System.out.println("Speed increased, current speed: " + speed);
    }
}
