package com.company.Scheme.details;

public class BreakPedal {

    private FrontWheels frontWheels;
    private RearWheels rearWheels;

    public BreakPedal(FrontWheels frontWheels, RearWheels rearWheels) {
        this.frontWheels = frontWheels;
        this.rearWheels = rearWheels;
    }

    public int decreaseSpeed(int speed) {
        frontWheels.decreaseSpeed();
        rearWheels.decreaseSpeed();
        return speed -= 10;
    }
}
