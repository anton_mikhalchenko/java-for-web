package com.company.Scheme.details;

public class ControlPanel {

    private int speed = 0;

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        if (speed < 0) {
            speed = 0;
        }
        else {
            this.speed = speed;
        }
    }

    public void currentSpeed() {
        System.out.println("Speed: " + speed);
    }
}
