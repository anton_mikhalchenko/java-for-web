package com.company.Scheme.details;

public class Engine implements StatusAware{

    private GasTank gasTank;
    private FrontWheels frontWheels;
    private RearWheels rearWheels;

    public Engine(GasTank gasTank, FrontWheels frontWheels, RearWheels rearWheels) {
        this.gasTank = gasTank;
        this.frontWheels = frontWheels;
        this.rearWheels = rearWheels;
    }

    public boolean works() {
        if (!gasTank.takeFuel()) {
            return false;
        }
        else {
            frontWheels.spin();
            rearWheels.spin();
            return true;
        }
    }

    @Override
    public void showStatus() {
        if (gasTank.getAmountOfFuel() == 0) {
            System.out.println("Engine doesn't work");
        }
        else {
            System.out.println("Engine works");
        }
    }
}
