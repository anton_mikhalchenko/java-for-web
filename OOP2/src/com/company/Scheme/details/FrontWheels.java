package com.company.Scheme.details;

public class FrontWheels implements Steared, Retarding {

    public void spin() {
        System.out.println("Front wheels spin");
    }

    @Override
    public void decreaseSpeed() {
        System.out.println("Front wheels fixed, speed decreased");
    }

    @Override
    public void turnLeft() {
        System.out.println("Front wheels turned left");
    }

    @Override
    public void turnRight() {
        System.out.println("Front wheels turned right");
    }

}
