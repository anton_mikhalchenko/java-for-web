package com.company.Scheme.details;

public class GasTank implements StatusAware{

    private int amountOfFuel = 10;

    public int getAmountOfFuel() {
        return amountOfFuel;
    }

    public void setAmountOfFuel(int amountOfFuel) {
        if (amountOfFuel <= 0) {
            throw new RuntimeException("Fuel cannot be less than 0");
        }
        else {
            this.amountOfFuel = amountOfFuel;
        }
    }

    public boolean takeFuel() {
        if (amountOfFuel == 0) {
            System.out.println("Gas tank is empty!");
            return false;
        }
        else {
            amountOfFuel--;
            return true;
        }
    }

    @Override
    public void showStatus() {
        System.out.println("Amount of fuel: " + amountOfFuel);
    }
}
