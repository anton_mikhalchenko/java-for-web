package com.company.Scheme.details;

public class HandBrake extends BreakPedal{


    public HandBrake(FrontWheels frontWheels, RearWheels rearWheels) {
        super(frontWheels, rearWheels);
    }

    @Override
    public int decreaseSpeed(int speed) {
        super.decreaseSpeed(speed);
        return speed -= 10;
    }
}
