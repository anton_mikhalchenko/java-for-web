package com.company.Scheme.details;

public class RearWheels implements Retarding {

    public void spin() {
        System.out.println("Rear wheels spin");
    }

    @Override
    public void decreaseSpeed() {
        System.out.println("Rear wheels fixed, speed decreased");
    }
}
