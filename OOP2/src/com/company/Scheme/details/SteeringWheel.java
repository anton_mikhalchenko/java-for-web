package com.company.Scheme.details;

public class SteeringWheel {

    private FrontWheels frontWheels;

    public SteeringWheel(FrontWheels frontWheels) {
        this.frontWheels = frontWheels;
    }

    public void turnRight() {
        frontWheels.turnRight();
    }

    public void turnLeft() {
        frontWheels.turnLeft();
    }
}
