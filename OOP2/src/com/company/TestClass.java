package com.company;

import com.company.Scheme.Vehicle;
import com.company.Scheme.Vehicles.*;

public class TestClass {

    public static void main(String[] args) {

        System.out.println("---Hatchback3Doors---");
        Hatchback3Doors hatchback3Doors = new Hatchback3Doors();
        test(hatchback3Doors);

        System.out.println("---Sedan---");
        Sedan sedan = new Sedan();
        test(sedan);

        System.out.println("---Hatchback5Doors---");
        Hatchback5Doors hatchback5Doors = new Hatchback5Doors();
        test(hatchback5Doors);

        System.out.println("---Coupe---");
        Coupe coupe = new Coupe();
        test(coupe);

        System.out.println("---SUV---");
        SUV suv = new SUV();
        test(suv);

        System.out.println("---Motorcycle---");
        Motorcycle motorcycle = new Motorcycle();
        test(motorcycle);
    }

    private static void test(Vehicle vehicle) {
        vehicle.startEngine();
        vehicle.accelerate();
        vehicle.turnLeft();
        vehicle.accelerate();
        vehicle.turnRight();
        for (int i = 0; i < 10; i++) {
            vehicle.accelerate();
        }
    }
}
