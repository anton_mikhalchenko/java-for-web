package com.company;

public enum AvailableShapes {
    CIRCLE, SQUARE, RECTANGLE, TRIANGLE
}
