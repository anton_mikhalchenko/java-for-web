package com.company;

public class Rectangle extends Shape {

    private double length;
    private double width;

    public Rectangle() {}

    public Rectangle(double length, double width) {
        setLength(length);
        setWidth(width);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double lenght) {
        if (lenght < 0) {
            throw new RuntimeException("Length cannot be less than 0");
        }
        else {
            this.length = lenght;
        }
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        if (width < 0) {
            throw new RuntimeException("Width cannot be less than 0");
        }
        else {
            this.width = width;
        }
    }

    @Override
    public double calculateArea() {
        return this.length * this.width;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (this.length + this.width);
    }
}
