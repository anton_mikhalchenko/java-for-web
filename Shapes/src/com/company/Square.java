package com.company;

public class Square extends Shape {

    private double side;

    public Square() {}

    public Square(double side) {
        setSide(side);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        if (side < 0) {
            throw new RuntimeException("Side cannot be less than 0");
        }
        else {
            this.side = side;
        }
    }

    @Override
    public double calculateArea() {
        return this.side * side;
    }

    @Override
    public double calculatePerimeter() {
        return 4 * this.side;
    }
}
