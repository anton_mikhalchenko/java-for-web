package com.company;

import java.util.Scanner;

public class TestClass {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Shape?: ");
        String stringShape = input.nextLine().toUpperCase();

        switch (AvailableShapes.valueOf(stringShape)) {
            case CIRCLE:
                Circle circle = new Circle();
                System.out.print("Enter the radius of the circle: ");
                circle.setRadius(input.nextDouble());
                System.out.println("For the circle with radius " + circle.getRadius());
                printAreaAndPerimeter(circle);
                break;

            case SQUARE:
                Square square = new Square();
                System.out.print("Enter a side of the square: ");
                square.setSide(input.nextDouble());
                System.out.println("For the square with side " + square.getSide());
                printAreaAndPerimeter(square);
                break;

            case TRIANGLE:
                Triangle triangle = new Triangle();
                System.out.print("Enter the first side of the triangle: ");
                triangle.setA(input.nextDouble());
                System.out.print("Enter the second side of the triangle: ");
                triangle.setB(input.nextDouble());
                System.out.print("Enter the third side of the triangle: ");
                triangle.setC(input.nextDouble());
                printAreaAndPerimeter(triangle);
                break;

            case RECTANGLE:
                Rectangle rectangle = new Rectangle();
                System.out.print("Enter length of the rectangle: ");
                rectangle.setLength(input.nextDouble());
                System.out.print("Enter width of the rectangle: ");
                rectangle.setWidth(input.nextDouble());
                System.out.println("For the rectangle with length " + rectangle.getLength() +
                        " and width " + rectangle.getWidth());
                printAreaAndPerimeter(rectangle);
                break;
        }

    }

    private static void printAreaAndPerimeter(Shape shape) {
        System.out.println("Area = " + shape.calculateArea());
        System.out.println("Perimeter = " + shape.calculatePerimeter());
    }
}
