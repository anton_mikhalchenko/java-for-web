package com.company;

public class Triangle extends Shape {

    private double a;
    private double b;
    private double c;

    public Triangle() {}

    public Triangle(double a, double b, double c) {
        setA(a);
        setB(b);
        setC(c);
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        if (a < 0) {
            throw new RuntimeException("Side cannot be less than 0");
        }
        else {
            this.a = a;
        }
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        if (b < 0) {
            throw new RuntimeException("Side cannot be less than 0");
        }
        else {
            this.b = b;
        }
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        if (c < 0) {
            throw new RuntimeException("Side cannot be less than 0");
        }
        else {
            this.c = c;
        }
    }

    @Override
    public double calculateArea() {
        double s = this.calculatePerimeter() / 2;
        double h = Math.sqrt(s * (s - this.a) * (s - this.b) * (s - this.c));
        return 0.5 * this.a * h;
    }

    @Override
    public double calculatePerimeter() {
        return this.a + this.b + this.c;
    }
}
