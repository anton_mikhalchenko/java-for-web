package com.company;

public class Boat extends Vehicle implements Driveable{

    public Boat(int amountOfFuel) {

        super(amountOfFuel);
    }

    @Override
    public void accelerate() {
        System.out.println("Wrummmmmm!");
        while (engine.takeGas(this.gasTank)) {
            engine.spinWheels(wheels);
            engine.printStatus(this.getSpeed(), gasTank.getFuel());
            this.setSpeed(this.getSpeed() + 3);
            gasTank.setFuel(gasTank.getFuel() - 1);
        }

        while (this.getSpeed() > 0) {
            brake();
        }
    }

    @Override
    public void brake() {
        this.setSpeed(this.getSpeed() - 1);
        engine.printStatus(this.getSpeed(), gasTank.getFuel());
        if (this.getSpeed() < 0) {
            this.setSpeed(0);
        }
    }

    @Override
    public void turn(String side) {
        while (this.getSpeed() > 10) {
            brake();
            gasTank.setFuel(gasTank.getFuel() - 1);
            engine.printStatus(this.getSpeed(), gasTank.getFuel());
            if (!engine.takeGas(this.gasTank)) {
                this.accelerate();
            }
        }
        System.out.println("You turned " + side);
    }
}
