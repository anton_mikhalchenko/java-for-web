package com.company;

public class Car extends Vehicle implements Driveable {

    public Car(int amountOfFuel) {

        super(amountOfFuel);
    }

    @Override
    public void accelerate() {
        System.out.println("Wrummmmmm!");
        while (engine.takeGas(this.gasTank)) {
            engine.spinWheels(wheels);
            engine.printStatus(this.getSpeed(), gasTank.getFuel());
            this.setSpeed(this.getSpeed() + 5);
            gasTank.setFuel(gasTank.getFuel() - 1);
        }

        while (this.getSpeed() > 0) {
            brake();
        }
    }

    @Override
    public void brake() {
        if (this.getSpeed() - 20 < 0) {
            this.setSpeed(0);
        }
        else {
            this.setSpeed(this.getSpeed() - 20);
        }
        engine.printStatus(this.getSpeed(), gasTank.getFuel());
    }

    @Override
    public void turn(String side) {
        while (this.getSpeed() > 20) {
            brake();
            gasTank.setFuel(gasTank.getFuel() - 1);
            engine.printStatus(this.getSpeed(), gasTank.getFuel());
            if (!engine.takeGas(this.gasTank)) {
                this.accelerate();
            }
        }
        System.out.println("You turned " + side);
    }
}
