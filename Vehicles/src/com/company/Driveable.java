package com.company;

public interface Driveable {
    void accelerate();
    void brake();
    void turn(String side);
}
