package com.company;

public class Engine {

    public boolean takeGas(GasTank gasTank) {

        if (gasTank.isEmpty()) {
            return false;
        }
        else {
            return true;
        }
    }

    public void spinWheels(Wheels wheels) {
        wheels.spin();
    }

    public void printStatus(int speed, int fuel) {
        System.out.println("Current speed: " + speed + ". Current gas: " + fuel);
        consoleWait(1000);
    }


    private void consoleWait(int millis) {
        try
        {
            Thread.sleep(millis);
        }
        catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
