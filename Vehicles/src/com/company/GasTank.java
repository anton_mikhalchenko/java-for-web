package com.company;

public class GasTank {

    private int fuel;

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        if (fuel < 0) {
            throw new RuntimeException("Fuel cannot be less than 0");
        }
        else {
            this.fuel = fuel;
        }
    }

    public boolean isEmpty() {
        if (this.fuel == 0) {
            System.out.println("Gas is over!");
            return true;
        }
        else {
            return false;
        }
    }
}
