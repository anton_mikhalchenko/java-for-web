package com.company;

public class SolarCar extends Car {

    private boolean sunBatteryOn;
    private boolean sunnyWeather;

    public SolarCar(int amountOfFuel, boolean sunnyWeather) {
        super(amountOfFuel);
        setSunnyWeather(sunnyWeather);
    }

    public boolean isSunBatteryOn() {
        return sunBatteryOn;
    }

    public void setSunBatteryOn(boolean sunBatteryOn) {
        this.sunBatteryOn = sunBatteryOn;
    }

    public boolean isSunnyWeather() {
        return sunnyWeather;
    }

    public void setSunnyWeather(boolean sunnyWeather) {
        this.sunnyWeather = sunnyWeather;
    }

    @Override
    public void accelerate() {
        if (this.isSunnyWeather()) {
            this.setSunBatteryOn(true);
            if (this.sunBatteryOn) {
                int startSpeed = this.getSpeed();
                while (this.getSpeed() < startSpeed + 20) {
                    engine.spinWheels(wheels);
                    this.setSpeed(this.getSpeed() + 5);
                    engine.printStatus(this.getSpeed(), gasTank.getFuel());
                }
            }
        }
        else {
            super.accelerate();
        }
    }
}
