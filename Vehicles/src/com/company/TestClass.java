package com.company;

import java.util.Scanner;

public class TestClass {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("What do you want to test(car, solar car or boat)?: ");
        String whatToTest = input.nextLine().toLowerCase();

        switch (whatToTest) {
            case "car":
                Car carWithSmallGasTank = new Car(10);
                carWithSmallGasTank.turn("Left");
                carWithSmallGasTank.accelerate();
                break;

            case "solar car":
                SolarCar solarCar = new SolarCar(10, true);
                solarCar.accelerate();
                solarCar.setSunnyWeather(false);
                solarCar.accelerate();
                break;

            case "boat":
                Boat boat = new Boat(10);
                boat.turn("Right");
                boat.accelerate();
        }

    }
}
