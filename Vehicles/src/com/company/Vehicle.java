package com.company;

public class Vehicle {

    Engine engine = new Engine();
    Wheels wheels = new Wheels();
    GasTank gasTank = new GasTank();
    private int speed = 0;

    public Vehicle(int amountOfFuel) {

        gasTank.setFuel(amountOfFuel);
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        if (speed < 0) {
            throw new RuntimeException("Speed cannot be less than 0");
        }
        else {
            this.speed = speed;
        }
    }
}
